import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private Matcher matcher;

    public static final String PHONE_PATTERN = "^\\+\\d{2}\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}$";
    public static final String NAME_PATTERN = "^[a-zA-Z]{3,15}$";
    public static final String NICKNAME_PATTERN = "^[a-zA-Z0-9_-]{3,15}$";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public boolean validate(String str, String regex){
        Pattern pattern = Pattern.compile(regex);
        matcher = pattern.matcher(str);
        return matcher.matches();
    }
}
