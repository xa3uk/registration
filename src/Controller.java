import java.util.Scanner;

public class Controller {
    private Account account;
    private View view;
    private Validator validator;

    public Controller(Account account, View view, Validator validator) {
        this.account = account;
        this.view = view;
        this.validator = validator;
    }

    public void process() {
        Scanner sc = new Scanner(System.in);
        String name = getAnswer(sc, Validator.NAME_PATTERN, View.ENTER_NAME);
        String surName = getAnswer(sc, Validator.NAME_PATTERN, View.ENTER_SURNAME);
        String nickname = getAnswer(sc, Validator.NICKNAME_PATTERN, View.ENTER_NICKNAME);
        String email = getAnswer(sc, Validator.EMAIL_PATTERN, View.ENTER_EMAIL);
        String phone = getAnswer(sc, Validator.PHONE_PATTERN, View.ENTER_PHONE);
        account.setName(name);
        account.setSurName(surName);
        account.setNickname(nickname);
        account.setEmail(email);
        account.setPhone(phone);
        view.printMessage(account.toString());
    }

    public String getAnswer(Scanner sc, String regex, String question) {
        view.printMessage(question);
        String answer = sc.nextLine();
        boolean valideInput = validator.validate(answer, regex);
        while (!valideInput) {
            view.printMessage(View.WRONG_INPUT_INT_DATA);
            answer = sc.nextLine();
            valideInput = validator.validate(answer, regex);
        }
        return answer;
    }
}
