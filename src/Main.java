public class Main {
    public static void main(String[] args) {
        Account account = new Account();
        View view = new View();
        Validator validator = new Validator();
        Controller controller = new Controller(account, view, validator);
        controller.process();
    }
}
