public class View {
    public static final String WRONG_INPUT_INT_DATA = "Wrong input! Repeat please!";
    public static final String ENTER_NAME = "Enter your name. Min 3 max 15 chars:";
    public static final String ENTER_SURNAME = "Enter your surname. Min 3 max 15 chars:";
    public static final String ENTER_NICKNAME = "Enter your nickname. Min 3 max 15 chars:";
    public static final String ENTER_EMAIL = "Enter your email:";
    public static final String ENTER_PHONE = "Enter your phone in format +__(___)___-__-__ :";

    public void printMessage(String message){
        System.out.println(message);
    }
}
